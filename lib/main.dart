import 'package:flutter/material.dart';
import 'package:flutter_dot_tab_indicator/dotTabIndicator.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        tabBarTheme: TabBarTheme(
          labelColor: Colors.black,
        ),
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Dot Tab Indicator'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Modify \'dotRadius\' to change radius\nModify \'borderside\' to change dot color\nModify \'insets\' to change dot position'
            ),
          ],
        ),
      ),
      bottomNavigationBar: DefaultTabController(
        length: 4,
        child: TabBar(
          indicator: DotTabIndicator(
            insets: EdgeInsets.only(bottom: 9),
            dotRadius: 2.5,
            borderSide: BorderSide(color: Colors.red)
          ),
            tabs: [
              Tab(
                child: Text("Tab1"),
              ),
              Tab(
                child: Text("Tab2"),
              ),
              Tab(
                child: Text("Tab3"),
              ),
              Tab(
                child: Text("Tab4"),
              ),
            ]
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
